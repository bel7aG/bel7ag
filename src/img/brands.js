import b1 from "./pictures/b1.png";
import b2 from "./pictures/b2.png";
import b3 from "./pictures/b3.jpg";
import b4 from "./pictures/b4.jpg";
import b5 from "./pictures/b5.png";
import b6 from "./pictures/b6.jpg";
import b7 from "./pictures/b7.jpg";
import b8 from "./pictures/b8.png";
import b9 from "./pictures/b9.png";
import b10 from "./pictures/b10.png";
import b11 from "./pictures/b11.png";
import b12 from "./pictures/b12.jpg";
import b13 from "./pictures/b13.jpg";
import b14 from "./pictures/b14.png";
import b15 from "./pictures/b15.png";
import b16 from "./pictures/b16.png";
import b17 from "./pictures/b17.jpg";

export {
  b1,
  b2,
  b3,
  b4,
  b5,
  b6,
  b7,
  b8,
  b9,
  b10,
  b11,
  b12,
  b13,
  b14,
  b15,
  b16,
  b17
};
