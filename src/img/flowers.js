import f1 from "./pictures/f1.jpg";
import f2 from "./pictures/f2.jpg";
import f3 from "./pictures/f3.jpg";
import f4 from "./pictures/f4.jpg";
import f5 from "./pictures/f5.jpg";
import f6 from "./pictures/f6.jpg";
import f7 from "./pictures/f7.jpg";
import f8 from "./pictures/f8.jpg";
import f9 from "./pictures/f9.jpg";

export { f1, f2, f3, f4, f5, f6, f7, f8, f9 };
