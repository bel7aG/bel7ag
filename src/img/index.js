import * as brands from "./brands";
import * as flowers from "./flowers";

import LogoPNG from "./pictures/Logo.png";
import CoffePNG from "./pictures/coffe.png";
import CoffeTwo from "./pictures/c2.jpg";
import CoffeThree from "./pictures/c3.jpg";
import CoffeFoor from "./pictures/c4.jpg";

import d2 from "./pictures/d2.jpg";
import d3 from "./pictures/d3.jpg";
import d4 from "./pictures/d4.jpg";
import d5 from "./pictures/d5.jpg";

export {
  brands,
  flowers,
  LogoPNG,
  CoffePNG,
  CoffeTwo,
  CoffeThree,
  CoffeFoor,
  d2,
  d3,
  d4,
  d5
};
