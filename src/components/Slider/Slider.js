import React, { useState, useEffect } from "react";
import { CoffePNG, CoffeTwo, CoffeThree, CoffeFoor } from "img";

const Slider = () => {
  const [pickedImage, setPickedImage] = useState(null);

  useEffect(() => {
    const images = [CoffePNG, CoffeTwo, CoffeThree, CoffeFoor];
    setPickedImage(images[Math.floor(Math.random() * 4)]);
  }, []);

  return (
    <div style={{ background: `url(${pickedImage})` }} className="slider" />
  );
};

export default Slider;
