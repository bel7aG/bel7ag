import React, { useState, useEffect, useRef } from "react";

const Post = ({ title, postDate, description, imgUrl }) => {
  const pRef = useRef(null);

  const [isReadMore, setIsReadMore] = useState(false);

  const [checkHeightMessage, setCheckHeightMessage] = useState(null);

  const [maxHeight, setMaxHeight] = useState(45);

  useEffect(() => {
    if (pRef.current) {
      setMaxHeight(pRef.current.scrollHeight);
    }
  }, []);

  useEffect(() => {
    if (pRef.current) {
      if (pRef.current.offsetHeight === 45) {
        setCheckHeightMessage("Read more");
      } else {
        setCheckHeightMessage("Read less");
      }
    }
  }, [isReadMore]);

  const handleReadMore = async () => {
    await setIsReadMore(!isReadMore);
  };

  return (
    <div className="post">
      <div>
        <div style={{ backgroundImage: `url(${imgUrl})` }} />
      </div>

      <div>
        <div>
          <h1>{title}</h1>
          <span>Posted: {postDate}</span>
        </div>
        <div>
          <p
            ref={pRef}
            style={{
              maxHeight: isReadMore ? maxHeight : 45
            }}
          >
            {description}
          </p>
          {maxHeight !== 45 && (
            <button onClick={handleReadMore}>{checkHeightMessage}</button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Post;
