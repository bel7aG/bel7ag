import React, { useState } from "react";
import { useWindowWidth } from "hooks";

const Sidebar = () => {
  const width = useWindowWidth();

  const [sidebarWidth, setSidebarWidth] = useState(width ? 20 : "28.7rem");

  const handleSideBar = () => {
    setSidebarWidth(sidebarWidth === 20 ? "28.7rem" : 20);
  };

  return (
    <div
      style={{
        width: width >= 768 ? "28.7rem" : sidebarWidth
      }}
      className="sidebar"
    >
      <div>
        <button
          style={{
            transform: `rotate(${sidebarWidth === 20 ? 180 : 0}deg)`
          }}
          onClick={handleSideBar}
          className="btn-player"
        >
          >
        </button>
      </div>

      <div>
        <div>
          <h2>NBC Shop</h2>
          <p>Your shopping cart is empty</p>
          <a href="#">Visit the shop</a>
        </div>

        <div>
          <h2>Next Event</h2>
          <p>
            <span>NORDIC BARISTA CUP 2011 </span>
            <span>Copenhagen,Denmark</span>
            <span>Dates : 25th - 27th August 2011</span>
            <span>Theme : SENSORY </span>
          </p>
          <a href="#">Sign up now.</a>
        </div>

        <div>
          <h2>Scoreboard</h2>
          <p>List of winners from past years</p>
          <ul>
            <li>2011 - ?</li>

            <li>2010 - Sweden</li>

            <li>2009 - Denmark</li>

            <li>2007 - Sweden</li>

            <li>2006 - Norway</li>

            <li>2005 - Norway</li>

            <li>2004 - Denmark</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
