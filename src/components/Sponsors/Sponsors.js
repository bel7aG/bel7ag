import React from "react";
import { brands } from "img";

const Sponsors = () => {
  const images = Object.values(brands).map((imgUrl, index) => (
    <article key={index}>
      <div key={index} style={{ backgroundImage: `url(${imgUrl})` }} />
    </article>
  ));

  return (
    <div className="sponsors">
      <h1>Nordic Barista Cup Sponsors</h1>
      <div className="grid">{images}</div>
    </div>
  );
};

export default Sponsors;
