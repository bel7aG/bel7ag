import React from "react";
import { Footer } from "components";
import "./_Layout.scss";

const Layout = ({ children }) => {
  return (
    <>
      <div className="layout">{children}</div>;
      <Footer />
    </>
  );
};

export default Layout;
