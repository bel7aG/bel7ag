import React from "react";
import { flowers } from "img";
import { SocialMedia } from "components";

const Footer = () => {
  const images = Object.values(flowers).map((flower, index) => (
    <div key={index} style={{ backgroundImage: `url(${flower})` }} />
  ));

  return (
    <footer>
      <div>
        <h1>About Nordic Barista Cup</h1>
        <p>The vision within the Nordic Barista Cup is:</p>
        <p className="light">
          “To create an environment in which knowledge about coffee and its
          sphere can be obtained”
        </p>
        <p>
          <span>’...create an environment…’</span>
          Combined with personally absorption having the opportunity to see and
          experience countries, people, traditions etc. will always serve as a
          source of inspiration in our daily work. The organization behind the
          Nordic Barista Cup see it as its main purpose to be a part of creating
          this forum in which people can meet, bond and achieve further
          knowledge.
        </p>
      </div>

      <div>
        <h1>NBC Flickr Stream</h1>
        <div className="grid">{images}</div>
      </div>

      <div>
        <h1>Contact</h1>
        <p className="light">Nordic Barista Cup</p>
        <p>
          <span>Amagertorv 13</span>
          <span>1160 Copenhagen K</span>
          <span>+45 33 12 04 28</span>
          <span>CVR: 11427693</span>
          <span>Email: bbrend@nordicbaristacup.com</span>
        </p>
        <SocialMedia />
      </div>
    </footer>
  );
};

export default Footer;
