import React from "react";
import {
  TwitterSVG,
  FacebookSVG,
  RssSVG,
  FeedburnerSVG,
  GmailSVG
} from "components";

const SocialMedia = () => {
  return (
    <div className="social">
      <a href="#">
        <TwitterSVG />
      </a>
      <a href="#">
        <FacebookSVG />
      </a>
      <a href="#">
        <RssSVG />
      </a>
      <a href="#">
        <FeedburnerSVG />
      </a>
      <a href="#">
        <GmailSVG />
      </a>
    </div>
  );
};

export default SocialMedia;
