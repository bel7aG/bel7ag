import React from "react";
import { SocialMedia } from "components";
const BackBar = () => {
  return (
    <div className="back-bar">
      <h1>
        “To create an environment in which knowledge about coffee and its sphere
        can be obtained”
      </h1>
      <SocialMedia />
    </div>
  );
};

export default BackBar;
