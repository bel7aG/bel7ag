import React, { useState } from "react";
import { Link } from "@reach/router";
import { useWindowWidth } from "hooks";
import { termCoolForLinks } from "helpers";

const navLinks = [
  "about nbc",
  "2011 event",
  "nordic roaster",
  "results",
  "links",
  "contact"
];

const Navigation = () => {
  const width = useWindowWidth();

  const [linksHeight, setLinksHeight] = useState(width ? 0 : 200);

  const links = navLinks.map((link, index) => (
    <li key={index}>
      <Link to={`/${termCoolForLinks(link)}`}>{link}</Link>
    </li>
  ));

  const handleShowMore = () => {
    setLinksHeight(linksHeight === 0 ? 200 : 0);
  };

  return (
    <nav className="navigation">
      <ul style={{ maxHeight: width <= 768 ? linksHeight : "100%" }}>
        {links}
      </ul>
      <button
        className="btn-player"
        style={{ transform: `rotate(${!linksHeight ? 90 : -90}deg)` }}
        onClick={handleShowMore}
      >
        >
      </button>
    </nav>
  );
};

export default Navigation;
