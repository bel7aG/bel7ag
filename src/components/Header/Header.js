import React from "react";
import { Link } from "@reach/router";
import { LogoPNG } from "img";
import { Navigation } from "components";

const Header = ({ children }) => {
  return (
    <header className="header">
      <div>
        {children}
        <div>
          <div>
            <Link to="/">
              <img src={LogoPNG} alt="" />
            </Link>
          </div>
          <Navigation />
        </div>
      </div>
    </header>
  );
};

export default Header;
