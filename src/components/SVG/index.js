import SearchSVG from "./SearchSVG";
import ArrowSVG from "./ArrowSVG";
import TwitterSVG from "./TwitterSVG";
import FacebookSVG from "./FacebookSVG";
import RssSVG from "./RssSVG";
import FeedburnerSVG from "./FeedburnerSVG";
import GmailSVG from "./GmailSVG";

export {
  SearchSVG,
  ArrowSVG,
  TwitterSVG,
  FacebookSVG,
  RssSVG,
  FeedburnerSVG,
  GmailSVG
};
