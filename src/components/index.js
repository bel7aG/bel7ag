import Layout from "./Layout";
import Header from "./Header";
import Navigation from "./Navigation";
import Slider from "./Slider";
import BackBar from "./BackBar";
import SocialMedia from "./SocialMedia";

import Posts from "./Posts";
import Post from "./Post";

import Sidebar from "./Sidebar";

import Sponsors from "./Sponsors";

import Footer from "./Footer";

export * from "./SVG";
export {
  Layout as default,
  Header,
  Navigation,
  Slider,
  BackBar,
  SocialMedia,
  Posts,
  Post,
  Sidebar,
  Sponsors,
  Footer
};
