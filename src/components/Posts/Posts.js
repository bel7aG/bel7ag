import React, { useState, useEffect } from "react";
import { Post } from "components";

const Posts = ({ data }) => {
  const [paginationX, setPaginationX] = useState({
    pixels: 0,
    counter: 0
  });

  const [page, setPage] = useState({
    page: 0,
    perPage: 3,
    data: []
  });

  useEffect(() => {
    handlePage(0);
  }, [data]);

  useEffect(() => {
    setPage({
      ...page,
      page: 0,
      data: pagesDecomposer()[page.page]
    });
  }, []);

  const pagesDecomposer = () => {
    var playerOne = [];
    let playerTwo = null;

    let pages = data
      .map((post, index) => {
        if (page.perPage === 1 && index === 0) {
          return [post];
        }
        if (index % page.perPage === 0 && index !== 0) {
          playerTwo = playerOne.length ? playerOne : [post];
          playerOne = [post];
          return playerTwo;
        } else {
          playerOne = [...playerOne, post];
          if (index === data.length - 1) {
            return playerOne;
          }
        }
      })
      .filter(element => element);

    if (page.perPage < data.length) {
      if (
        (page.perPage % 2 === 0 && (data.length - 1) % 2) === 0 ||
        (page.perPage % 2 !== 0 && (data.length - 1) % 2) === 0
      ) {
        pages = [...pages, [data[data.length - 1]]];
      }
    }

    return pages || [];
  };

  const handlePage = pageIndex => {
    setPage({
      ...page,
      page: pageIndex,
      data: pagesDecomposer()[pageIndex || 0] || []
    });
  };

  const handleNextPage = () => {
    setPaginationX({
      pixels:
        paginationX.counter >= pagesDecomposer().length - 3
          ? 0
          : paginationX.pixels - 42,
      counter:
        paginationX.counter >= pagesDecomposer().length - 3
          ? 0
          : paginationX.counter + 1
    });
  };

  return (
    <div className="posts">
      {page.data.length ? (
        <div>
          <div>
            {page.data.map(postData => (
              <Post key={postData.id} {...postData} />
            ))}
          </div>
          <div>
            <ul>
              <li>
                <button>
                  Page {page.page + 1} Of {pagesDecomposer().length}
                </button>
              </li>
              <li>
                <ul
                  style={{ transform: `translateX(${paginationX.pixels}px)` }}
                >
                  {pagesDecomposer().map((blockOfPages, pageIndex) => (
                    <li key={pageIndex}>
                      <button
                        key={pageIndex}
                        onClick={() => handlePage(pageIndex)}
                        style={{
                          backgroundColor:
                            pageIndex === page.page ? "#C1C1C1" : "#eee",
                          color: pageIndex === page.page ? "#fff" : "#9a9da1"
                        }}
                      >
                        {pageIndex + 1}
                      </button>
                    </li>
                  ))}
                </ul>
              </li>
              <li>
                {pagesDecomposer().length >= 3 && (
                  <button onClick={handleNextPage}>
                    <span>&#62;&#62;</span>
                  </button>
                )}
              </li>
            </ul>
          </div>
        </div>
      ) : (
        <div className="empty">
          <h1>No Item found!</h1>
        </div>
      )}
    </div>
  );
};

export default Posts;
