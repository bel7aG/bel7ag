import { d2, d3, d4, d5 } from "img";

export const data = [
  {
    id: 1,
    title: `Wonderful Copenhagen 2011`,
    postDate: `23/1-2011`,
    description: `The aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind ohe aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory...`,
    imgUrl: d2
  },
  {
    id: 2,
    title: `Nordic Barista Cup 2011 in Copenhagen`,
    postDate: `22/1-2011`,
    description: `Nordic Barista Cup 2011 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page....`,
    imgUrl: d4
  },
  {
    id: 3,
    title: `2010 Winners: Sweden`,
    postDate: `12/1-2011`,
    description: `Oh my goodness, the final night is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible locnight is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...`,
    imgUrl: d3
  },
  {
    id: 4,
    title: `This is Forth one`,
    postDate: `23/1-2011`,
    description: `The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat, fugit. Unde tenetur quaerat illum quis sunt culpa possimus, perspiciatis odio accusantium doloremque. Explicabo non quibusdam recusandae voluptas nemo unde odio.
    Beatae, cupiditate. Voluptates minus exercitationem harum earum suscipit iste unde, saepe sit quibusdam itaque nemo tenetur corrupti temporibus porro dolore recusandae voluptate maiores nobis eius ab quae qui nulla blanditiis.
    Consequuntur, quos fugiat minus sint officiis laboriosam doloremque qui sunt! Asperiores natus, fugit optio ad at eos cupiditate, quasi laborum sint neque velit, autem suscipit obcaecati officiis ea doloremque. Qui?
    Pariatur error soluta ullam architecto aperiam culpa, itaque mollitia voluptas obcaecati iste nesciunt minima placeat labore asperiores aliquam sed repellendus minus saepe fuga facilis? At, saepe itaque! Error, consectetur libero?`,
    imgUrl: d4
  },
  {
    id: 5,
    title: `Cup 2011 in Copenhagen`,
    postDate: `22/1-2011`,
    description: `Nordic Barista Cup 2011 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore ab dolorum laboriosam esse similique, optio deserunt at voluptas. Ipsa iure beatae amet aperiam molestias est perferendis maiores rerum maxime saepe.
    Error, optio enim doloremque omnis ullam voluptates, ab dicta minus debitis modi eaque molestias quo, quod doloribus. Minima ducimus commodi sapiente incidunt similique eaque pariatur, ipsum, eos quos voluptatem vitae.
    Placeat nesciunt iusto ipsum perferendis dolorum unde facilis voluptatum a, asperiores delectus veniam tempora magni maiores ad odit dolore numquam, amet voluptates! Doloremque, in nobis. Similique porro sequi officia libero.
    Voluptates ab porro, modi adipisci quibusdam pariatur consequatur nihil veniam laudantium non, necessitatibus culpa atque repellat dolorem dolorum quo corrupti excepturi, officia dolor rerum? Dolore voluptatibus eligendi delectus inventore quos!`,
    imgUrl: d4
  },
  {
    id: 6,
    title: `2008 Winners: Sweden`,
    postDate: `12/1-2011`,
    description: `Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Magni aliquam ipsam tenetur doloremque perspiciatis fugit tempore dolorem nihil quibusdam a beatae facere cupiditate, hic vel incidunt quae, unde blanditiis quidem?`,
    imgUrl: d5
  },
  {
    id: 7,
    title: `Copenhagen 2020`,
    postDate: `23/1-2011`,
    description: `The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory...`,
    imgUrl: d4
  },
  {
    id: 8,
    title: `Cup 2023 in Copenhagen`,
    postDate: `22/1-2011`,
    description: `Nordic Barista Cup 2011 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam, similique alias aut iure a maxime voluptas, numquam fugiat temporibus non illum ipsa officiis sed maiores dicta rem dolorum quae deleniti.Eum expedita in illo aperiam dolorem possimus dolore doloremque recusandae quisquam nobis molestiae adipisci obcaecati neque, est temporibus molestias accusamus cum autem! Voluptatem reiciendis molestiae magni quae perspiciatis itaque voluptate.`,
    imgUrl: d3
  },
  {
    id: 9,
    title: `2010 Winners: Sweden`,
    postDate: `12/1-2011`,
    description: `Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat reiciendis repellat similique nemo eligendi quod vero, quasi quia quos ad voluptatum dolorem ea corrupti aliquam architecto cumque eum doloremque nobis!
    Earum molestiae aperiam consequuntur laudantium illum iusto esse alias laboriosam quibusdam nisi cum est repudiandae soluta ullam quam voluptatum neque totam harum at, laborum nam. Exercitationem provident dolore consequatur assumenda?
    Quae omnis accusantium officia numquam minima fugit eius quasi consequuntur quod nemo fuga autem, unde odit delectus non laudantium, voluptatem, facere facilis asperiores enim ipsam similique. Veritatis dignissimos vero in!
    Minima necessitatibus, aliquid ipsum accusamus perferendis quo. Nemo veritatis dolores nam quia, quos vero ad distinctio nesciunt vel, minus molestiae repudiandae ut tempora modi itaque quod! Deserunt dolores blanditiis modi.
    Nesciunt reprehenderit magnam hic placeat perspiciatis, quis, error veritatis adipisci quaerat, beatae molestiae dolorem? Eius, eaque omnis. Reiciendis cupiditate neque quam nihil, quis magni sed, molestias ducimus natus aliquid voluptate.
    Necessitatibus magnam, reiciendis debitis ullam rem omnis cumque doloribus facilis eaque veritatis non sed provident! Repellendus natus minima impedit aut consectetur? Velit, autem est? Magni reiciendis ab excepturi corporis ea!`,
    imgUrl: d2
  },
  {
    id: 10,
    title: `Wonderful Copenhagen 2011`,
    postDate: `23/1-2011`,
    description: `The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory...`,
    imgUrl: d3
  },
  {
    id: 11,
    title: `Nordic Barista Cup 2011 in Copenhagen`,
    postDate: `22/1-2011`,
    description: `Nordic Barista Cup 2011 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page....`,
    imgUrl: d2
  },
  {
    id: 12,
    title: `2010 Winners: Sweden`,
    postDate: `12/1-2011`,
    description: `Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima sequi rerum fugit excepturi inventore saepe, perferendis enim animi accusantium commodi amet. Quo iusto placeat dicta suscipit. Blanditiis voluptates quibusdam nobis!
    Explicabo voluptatem veritatis sed aliquid voluptate pariatur culpa in minima aut fuga quod provident, laudantium beatae numquam voluptatibus laborum aperiam alias reprehenderit consectetur labore velit dignissimos! Consequatur nam accusamus amet!
    Fuga accusantium et saepe consequatur rerum quos quisquam provident, quod laboriosam deleniti eius temporibus nobis delectus numquam aliquid ratione ut, facilis consequuntur omnis pariatur. Ab, praesentium! Totam nemo beatae porro?
    Placeat doloremque dolore perspiciatis ipsa excepturi magni hic in incidunt obcaecati quae. Officiis assumenda quaerat consequuntur possimus facilis culpa consequatur laboriosam laborum saepe. Facere perferendis deserunt laudantium consectetur natus blanditiis.`,
    imgUrl: d4
  },

  {
    id: 13,
    title: `2010 Winners: Sweden`,
    postDate: `12/1-2011`,
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium, facilis suscipit? Deleniti blanditiis asperiores molestias quasi, autem voluptas unde placeat nostrum dicta illo, cum nesciunt architecto repellat vero dolorum veniam!
      Neque iusto quam beatae suscipit enim nobis commodi temporibus nemo aspernatur placeat earum ratione quod quasi laboriosam voluptatum fugiat et reprehenderit dicta, animi cum dolorem tempora aliquam. Quibusdam, rem dolores.
      Amet perspiciatis non animi quibusdam aut quasi provident repudiandae expedita inventore iure consequuntur voluptatum corporis, fugit voluptate laudantium. Nostrum, voluptates perspiciatis eos commodi quisquam distinctio aliquam reprehenderit modi eaque fuga?
      Ex repellat ab nulla odit labore consequatur inventore vero unde nam deleniti, harum facilis nemo quam sapiente fuga? Quisquam harum tempora praesentium natus recusandae eveniet dolor non illum fugit quos.
      Quibusdam eaque odit vitae doloribus neque cupiditate earum possimus! Doloribus alias nesciunt, magni corporis labore ex! Dolorum aliquid ipsam hic labore nisi! Cumque veniam incidunt iusto nemo maiores ducimus libero!
      Aut necessitatibus fugit molestias eligendi sunt quasi, at, magnam sint nihil maiores qui fugiat nulla eaque dolorum deserunt distinctio rem voluptatibus tenetur atque quas. Porro quaerat tempora beatae odit ut!
      Blanditiis magnam, consequatur iusto vero error natus hic? Animi a esse eum praesentium molestias rem ea ex necessitatibus similique recusandae exercitationem, labore dolorum autem! Ipsam error ipsa placeat qui dolorum.
      Magnam facere dolorem distinctio architecto at reprehenderit provident? Fugit vitae, quidem non deserunt impedit excepturi asperiores fuga eaque rerum commodi facere aliquid expedita nam odio culpa quis obcaecati esse placeat!
      Pariatur nemo autem eius vitae? Aliquam fugit fuga doloribus eius dolore sint non molestias, excepturi ullam voluptatibus natus incidunt modi quibusdam porro cum dolorum officiis iste autem nulla debitis ipsum!
      Quam eaque est fugiat! Ratione optio commodi cumque similique sint ipsa ea omnis praesentium, eius tempore facere animi sapiente fugiat quas ipsum iure quis accusantium voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus voluptates eum modi voluptas doloribus.
      Ducimus fugiat, asperiores soluta enim, maxime obcaecati vitae nulla alias  maxime obcaecati vitae nulla alias  maxime obcaecati vitae nulla alias  maxime obcaecati vitae nulla alias  maxime obcaecati vitae nulla alias  maxime obcaecati vitae nulla alias non ipsa sit quia reprehenderit eveniet eaque facilis! Provident quisquam quo impedit labore? Repudiandae sint commodi id laboriosam, possimus reiciendis.
      Quas quos ipsa soluta illum explicabo tempora facere cumque! Ut earum suscipit porro vel voluptatem nulla aliquam minus odit. Dolor quod consequuntur nemo, eveniet sed accusantium voluptate ad pariatur alias?
      Quidem voluptates perspiciatis excepturi, neque deserunt distinctio ipsam incidunt quod molestias voluptas architecto sequi itaque provident aspernatur minus qui impedit amet numquam officia alias nihil atque enim quasi. Eum, vero!
      Nihil vitae maiores voluptatibus perspiciatis placeat magni, ipsa reiciendis veritatis error, saepe, corporis nam dolorem alias maxime. Iste maiores libero harum totam ut ipsa quis, aspernatur sequi autem sit at!   Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...`,
    imgUrl: d2
  }
];
