import { useState, useEffect } from "react";

let defaultWidth;

if (typeof window !== `undefined`) {
  defaultWidth = window.innerWidth;
}

export default () => {
  const [width, setWidth] = useState(defaultWidth);

  useEffect(() => {
    const handleResizeWidth = () => setWidth(window.innerWidth);

    window.addEventListener("resize", handleResizeWidth);

    return () => {
      window.removeEventListener("resize", handleResizeWidth);
    };
  }, []);

  return width;
};
