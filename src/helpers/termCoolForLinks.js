export default term => term.replace(/[^\ba-zA-Z.\d]+/g, "-").toLowerCase();
