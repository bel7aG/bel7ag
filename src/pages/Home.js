import React, { useState, useEffect } from "react";
import {
  Header,
  Slider,
  BackBar,
  Posts,
  Sidebar,
  Sponsors,
  SearchSVG,
  ArrowSVG
} from "components";
import { data } from "../components/Posts/data";

const Home = () => {
  const [searchText, setSearchText] = useState("");
  const [dataFilter, setDataFitler] = useState(data);

  useEffect(() => {
    setDataFitler([
      ...data.filter(post =>
        searchText
          ? post.title.toLowerCase().includes(searchText.toLowerCase()) ||
            post.description.toLowerCase().includes(searchText.toLowerCase())
          : post
      )
    ]);
  }, [searchText]);

  const handleSearchChange = event => {
    setSearchText(event.target.value);
  };

  return (
    <div className="home">
      <Header>
        <div className="search-box">
          <div>
            <SearchSVG />
            <ArrowSVG />
            <input
              value={searchText}
              type="text"
              onChange={handleSearchChange}
              placeholder="Search..."
            />
          </div>
        </div>
      </Header>
      <Slider />
      <BackBar />
      <main>
        <Posts data={dataFilter} />
        <Sidebar />
      </main>
      <Sponsors />
    </div>
  );
};

export default Home;
