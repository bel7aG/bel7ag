import React from "react";
import { Location } from "@reach/router";
import { Header } from "components";

const Page = () => {
  return (
    <div className="page">
      <Header>
        <div />
      </Header>
      <Location>
        {({ location: { pathname } }) => {
          return <h1>this is {pathname} page</h1>;
        }}
      </Location>
    </div>
  );
};

export default Page;
