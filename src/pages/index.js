import React from "react";
import { Router } from "@reach/router";
import Layout from "components";

import NotFound from "./NotFound";
import Home from "./Home";
import Page from "./Page";

import "../scss/index.scss";

const AppRouter = props => (
  <Layout>
    <Router>
      <Home path="/" />
      <Page path="/:page" />
      <NotFound default />
    </Router>
  </Layout>
);

export default AppRouter;
